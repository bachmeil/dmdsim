Create a system of equations in R, then estimate the equations and do a simulation in D. Main advantages:

- Enforces consistency between the coefficients estimated in R and their use in the simulations in D.
- Can handle very general dynamic system specifications.